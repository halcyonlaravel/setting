<?php

namespace HalcyonLaravelBoilerplate\Setting\Tests;

use HalcyonLaravelBoilerplate\Setting\Http\Controllers\API\ValueStoreController;
use HalcyonLaravelBoilerplate\Setting\SettingFacade;
use Storage;

class APIControllerTest extends TestCase
{
    /** @test */
    public function get_text_success()
    {
        $localDiskName = 'test-disk-local';
        Storage::fake($localDiskName);

        $name = 'test-name';
        $field = 'test-name';

        config(
            [
                'setting.disk_names.file' => $localDiskName,
                'setting.modules' => [
                    $name => [
                        'fields' => [
                            $field => [
                                'type' => 'text',
                                'validations' => 'required|string',
                            ],
                        ],
                    ],
                ],
            ]
        );

        $value = [$field => 'value_1'];

        SettingFacade::make($name)->put($value);

        $this->assertEquals($value, (new ValueStoreController())->get($name));
    }
//    /** @test */
//    public function get_file_success()
//    {
//
//        $now = now();
//        TestTime::freeze($now);
//
//        $localDiskName = 'test-disk-local';
//        $publicDiskName = 'public';
//
//        Storage::fake($publicDiskName);
//        Storage::fake($localDiskName);
//    }
}