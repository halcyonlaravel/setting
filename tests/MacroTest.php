<?php

namespace HalcyonLaravelBoilerplate\Setting\Tests;

use HalcyonLaravelBoilerplate\Setting\SettingFacade;

class MacroTest extends TestCase
{
    /** @test */
    public function api_route()
    {
        SettingFacade::apiRoutes();

        $this->assertEquals(url('test-name'), route('get', 'test-name'));
    }
}