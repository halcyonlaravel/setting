<?php

namespace HalcyonLaravelBoilerplate\Setting\Tests\Helpers;

use HalcyonLaravelBoilerplate\Setting\ValueStore\ConversionContract;
use Spatie\Image\Image;

class TestConversion implements ConversionContract
{

    public static function run(Image $image, string $valueStore, string $field): Image
    {
        return $image;
    }
}