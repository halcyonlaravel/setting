<?php

namespace HalcyonLaravelBoilerplate\Setting\Tests;

use HalcyonLaravelBoilerplate\Setting\SettingServiceProvider;
use Orchestra\Testbench\TestCase as Orchestra;
use Storage;

class TestCase extends Orchestra
{
    public function setUp(): void
    {
        parent::setUp();

        foreach (array_keys(config('filesystems.disks')) as $diskName) {
            Storage::fake($diskName);
        }
    }

    protected function getPackageProviders($app): array
    {
        return [
            SettingServiceProvider::class,
        ];
    }
}
