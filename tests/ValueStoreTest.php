<?php

namespace HalcyonLaravelBoilerplate\Setting\Tests;

use HalcyonLaravelBoilerplate\Setting\SettingFacade;
use HalcyonLaravelBoilerplate\Setting\Tests\Helpers\TestConversion;
use Illuminate\Http\UploadedFile;
use Spatie\TestTime\TestTime;
use Storage;

class ValueStoreTest extends TestCase
{
    /** @test */
    public function put_value_store()
    {
        $localDiskName = 'test-disk-local';

        Storage::fake($localDiskName);
        config(['setting.disk_names.file' => $localDiskName]);

        SettingFacade::make('test-name')->put(['test_key' => 'test_value']);

        Storage::disk($localDiskName)->assertExists('test-name.json');

        $this->assertEquals(['test_key' => 'test_value'], SettingFacade::make('test-name')->all());
    }

    /** @test */
    public function put_value_image()
    {
        $now = now();
        TestTime::freeze($now);

        $localDiskName = 'test-disk-local';
        $publicDiskName = 'public';

        Storage::fake($publicDiskName);
        Storage::fake($localDiskName);


        $key = 'test-key';
        $name = 'test-name';

        config(
            [
                "filesystems.disks.$publicDiskName.url" => 'https://test',
                'setting.disk_names.file' => $localDiskName,
                'setting.modules' => [
                    $name => [
                        'fields' => [
                            $key => [
                                'type' => 'text',
                                'validations' => 'required|string',
                                'register_conversions' => TestConversion::class,
                            ],
                        ],
                    ],
                ],
            ]
        );

        $filename = 'test-file.jpg';

        $result = SettingFacade::make($name)->upload(UploadedFile::fake()->image($filename), $key);

        $this->assertEquals(
            DIRECTORY_SEPARATOR.
            $name.DIRECTORY_SEPARATOR.$key.DIRECTORY_SEPARATOR.'conversions'.DIRECTORY_SEPARATOR.$filename.
            '?'.http_build_query(['v' => $now->timestamp]),
            $result
        );

        Storage::disk($publicDiskName)->assertExists($name.DIRECTORY_SEPARATOR.$key.DIRECTORY_SEPARATOR.$filename);
    }
}
