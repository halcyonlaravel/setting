# Changelog

All notable changes to `setting` will be documented in this file.

## v1.0.3 - 2021-06-25

- Fix conversion

## v1.0.1|v1.0.2 - 2021-06-22

- Add controller, route for API

## v1.0.0 - 2021-06-22

- initial release
