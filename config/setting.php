<?php

return
    [
        /**
         *
         *
         * filesystem:
         *
         * 'value_store_local' => [
         * 'driver' => 'local',
         * 'root' => storage_path('app/value-store'),
         * ],
         * 'value_store_public' => [
         * 'driver' => 'local',
         * 'root' => storage_path('app/public/value-store'),
         * 'url' => env('APP_URL').'/storage-value-store',
         * 'visibility' => 'public',
         * ],
         *
         *     'links' => [
         *
         * public_path('storage-value-store') => storage_path('app/public/value-store'),
         * ],
         *
         */
        'disk_names' => [
            'image' => env('SETTING_PUBLIC_DISK_NAME', 'value_store_public'),
            'file' => env('SETTING_LOCAL_DISK_NAME', 'value_store_local'),
        ],
        'modules' => [
            'test' => [
                'fields' => [
                    'test_field_a' => [
                        'type' => 'text',
                        'validations' => 'required|string',
                    ],
                    'test_field_b' => [
                        'type' => 'textarea-ckeditor',
                        'validations' => 'required|string',
                    ],
                ],
            ],
        ],
    ];
