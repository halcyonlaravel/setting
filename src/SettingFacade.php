<?php

namespace HalcyonLaravelBoilerplate\Setting;

use HalcyonLaravelBoilerplate\Setting\ValueStore\CoreValueStore;
use Illuminate\Support\Facades\Facade;

/**
 * @mixin CoreValueStore|\HalcyonLaravelBoilerplate\Setting\MacroMixin
 */
class SettingFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return CoreValueStore::class;
    }
}
