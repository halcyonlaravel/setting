<?php

use HalcyonLaravelBoilerplate\Setting\SettingFacade;

if (! class_exists('ValueStore')) {
    /**
     * @deprecated must be replace \HalcyonLaravelBoilerplate\Setting\SettingFacade or Setting
     */
    class ValueStore extends SettingFacade
    {
    }
}

