<?php

namespace HalcyonLaravelBoilerplate\Setting;

use Closure;
use HalcyonLaravelBoilerplate\Setting\Http\Controllers\API\ValueStoreController;
use Route;

class MacroMixin
{
    public function apiRoutes(): Closure
    {
        return function () {
            Route::get('{valueStore}', [ValueStoreController::class, 'get'])->name('get');
        };
    }
}