<?php

namespace HalcyonLaravelBoilerplate\Setting\ValueStore;

use Closure;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Traits\Macroable;
use Spatie\Image\Image;
use Spatie\Image\Manipulations;
use Spatie\Valuestore\Valuestore;

final class CoreValueStore extends Valuestore
{
    use Macroable;

    private string $valueStoreName;

    /**
     * add this to make this capable of registering facade
     * dont make instance with this,
     * just use make()
     */
    public function __construct()
    {
        parent::__construct();
    }

    public static function make(string $fileName, array $values = null): self
    {
        $prefix = self::getFileStorage()
            ->getAdapter()
            ->getPathPrefix();

        $v = parent::make($prefix.$fileName.'.json', $values);
        $v->valueStoreName = $fileName;
        return $v;
    }

    public function flush()
    {
        $this->getImageStorage()->deleteDirectory($this->valueStoreName);
        return parent::flush();
    }

    public static function config(string $key, $default = null)
    {
        return config("setting.$key", $default);
    }

    private static function getImageStorage(): Filesystem
    {
        return Storage::disk(self::getImageDiskName());
    }

    private static function getFileStorage(): Filesystem
    {
        return Storage::disk(self::getFileDiskName());
    }

    private static function getFileDiskName(): string
    {
        return self::config('disk_names.file');
    }

    private static function getImageDiskName(): string
    {
        return self::config('disk_names.image');
    }

    /**
     * @param  \Illuminate\Http\UploadedFile  $image
     * @param  string|\Closure  $keyPath
     * @param  string|null  $closureKey
     *
     * @return string|string[]|null
     */
    public function upload(UploadedFile $image, $keyPath, string $closureKey = null): string
    {
        if (! is_string($keyPath) && ! $keyPath instanceof Closure) {
            abort(500, 'must be string or closure for 2nd parameter in value store upload.');
        }

        $storeName = $this->valueStoreName;

        $prefixDirectory = $this->getImageStorage()
            ->getAdapter()
            ->getPathPrefix();

        [$fileName] = explode('.', $image->getClientOriginalName());
        $extension = $image->getClientOriginalExtension();


        $path = is_string($keyPath)
            ? $keyPath
            : (
                $closureKey ??
                abort(500, sprintf('%s::%s() required 3rd param', self::class, 'upload'))
            );
        $baseDir = $prefixDirectory.$storeName.DIRECTORY_SEPARATOR.str_replace('_', '-', $path).DIRECTORY_SEPARATOR;
        unset($path);

        $baseDirConverted = $baseDir.'conversions'.DIRECTORY_SEPARATOR;

        File::deleteDirectory($baseDir);
        File::makeDirectory($baseDir, 0755, true, true);
        File::makeDirectory($baseDirConverted, 0755, true, true);

        Image::load($image)
            ->save($baseDir.$image->getClientOriginalName());

        if ($extension == 'ico') {
            $final = $baseDir.$image->getClientOriginalName();
        } else {
//            if ($extension != Manipulations::FORMAT_PNG) {
//                $extension = Manipulations::FORMAT_JPG;
//            }
            $convertedPath = $baseDirConverted.$fileName.'.'.$extension;

            if (is_string($keyPath)) {
                $converter = self::config("modules.$storeName.fields.$keyPath.register_conversions");

                $interfaces = class_implements($converter);
                if (blank($interfaces) || ! in_array(ConversionContract::class, $interfaces)) {
                    abort(500, "`$converter` must implement ".ConversionContract::class);
                }
                unset($interfaces);

                /** @var \Spatie\Image\Image $c */
                $c = (new $converter())::run(Image::load($image), $storeName, $keyPath);
            } else {
                $c = $keyPath(Image::load($image));
            }

            $c->manipulate(
                function (Manipulations $manipulations) {
                    if (config()->has('media-library.image_optimizers')) {
                        $manipulations->optimize(config('media-library.image_optimizers'));
                    }
                }
            )
                ->save($convertedPath);

            $final = $convertedPath;
        }

        return self::generateLink($prefixDirectory, $final).'?'.http_build_query(['v' => now()->timestamp]);
    }

    public function deleteUpload(string $fieldName): void
    {
        $fieldName = str_replace('_', '-', $fieldName);
        $this->getImageStorage()->deleteDirectory($this->valueStoreName.DIRECTORY_SEPARATOR.$fieldName);
    }

    private function generateLink(string $prefixDirectory, string $file): string
    {
        $urlFromFilesystem = config("filesystems.disks.{$this->getImageDiskName()}.url");

        $url = parse_url($urlFromFilesystem);

        return str_replace("{$url['scheme']}://{$url['host']}", '', $urlFromFilesystem)
            .DIRECTORY_SEPARATOR.
            str_replace($prefixDirectory, '', $file);
    }
}
