<?php

namespace HalcyonLaravelBoilerplate\Setting\ValueStore;

interface ValidationContract
{
    public static function rules(): array;
}