<?php

namespace HalcyonLaravelBoilerplate\Setting\ValueStore;

use Spatie\Image\Image;

interface ConversionContract
{
    public static function run(Image $image, string $valueStore, string $field): Image;

}