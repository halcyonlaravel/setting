<?php

namespace HalcyonLaravelBoilerplate\Setting\Http\Controllers\API;

use HalcyonLaravelBoilerplate\CoreBase\ValueStore\ConversionContract;
use HalcyonLaravelBoilerplate\Setting\Http\Controllers\Controller;
use HalcyonLaravelBoilerplate\Setting\SettingFacade as Setting;

class ValueStoreController extends Controller
{
    public function get(string $valueStore): array
    {
        if (! in_array($valueStore, array_keys(Setting::config('modules')))) {
            abort(404);
        }

        $values = collect(Setting::make($valueStore)->all())->map(
            function (?string $value, $key) use ($valueStore) {
                $fieldType = Setting::config("modules.$valueStore.fields.$key.type");
                $registerConversions = Setting::config(
                    "modules.$valueStore.fields.$key.register_conversions"
                );

                if ($fieldType == 'file' && (new $registerConversions()) instanceof ConversionContract) {
                    return url($value);
                } elseif ($fieldType == 'money') {
                    return $value / 100;
                }

                return $value;
            }
        );

        $fields = array_keys(Setting::config("modules.$valueStore.fields"));

        $values = $values->filter(fn(?string $item, string $key) => in_array($key, $fields));

        foreach ($fields as $field) {
            if (! $values->contains(fn(?string $item, string $key) => $key == $field)) {
                $values->put($field, null);
            }
        }

        return $values->toArray();
    }
}
