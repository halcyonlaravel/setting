<?php

namespace HalcyonLaravelBoilerplate\Setting;

use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;

class SettingServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        $package
            ->name('setting')
            ->hasConfigFile();
    }

    /**
     * @throws \ReflectionException
     */
    public function packageRegistered()
    {
        SettingFacade::mixin(new MacroMixin());
    }
}
