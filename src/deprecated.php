<?php

namespace HalcyonLaravelBoilerplate\CoreBase\ValueStore;

use HalcyonLaravelBoilerplate\Setting\ValueStore\ConversionContract as newConversionContract;
use HalcyonLaravelBoilerplate\Setting\ValueStore\ValidationContract as newValidationContract;

if (! class_exists('ValidationContract')) {
    /**
     * @deprecated must be replace \HalcyonLaravelBoilerplate\Setting\ValueStore\ValidationContract
     */
    interface ValidationContract extends newValidationContract
    {
    }
}

if (! class_exists('ConversionContract')) {
    /**
     * @deprecated must be replace \HalcyonLaravelBoilerplate\Setting\ValueStore\ConversionContract
     */
    interface ConversionContract extends newConversionContract
    {
    }
}

